using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField] UIDocument _uIDocument;

    Slider _sliderMusicVolume;
    Button _btnStartGame;
    Button _btnQuit;

    void Start()
    {
        _sliderMusicVolume =  _uIDocument.rootVisualElement.Q<Slider>("volumeSlider");
        _sliderMusicVolume.value = Settings.Instance.MusicVolume;
        _sliderMusicVolume.RegisterValueChangedCallback(v => {
            Settings.Instance.MusicVolume = v.newValue;
        });

        _btnStartGame = _uIDocument.rootVisualElement.Q<Button>("btnStartGame");
        _btnStartGame.clicked += delegate{
            Time.timeScale = 1f;
            SceneManager.LoadScene("Game");
        };

        _btnQuit = _uIDocument.rootVisualElement.Q<Button>("btnQuit");
        _btnQuit.clicked += delegate{
            Application.Quit();
        };
    }
}
