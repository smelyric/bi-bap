using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(Player))]
public class Manager : MonoBehaviour
{
    public static Manager Instance;

    Camera _mainCamera;
    public Camera MainCamera { get { return _mainCamera; } }

    Player _player;
    public Player Player { get { return _player; } }

    [SerializeField] GameObject _rangeIndicatorPrefab;
    public GameObject RangeIndicatorPrefab { get { return _rangeIndicatorPrefab; } }

    [SerializeField] MainUI _mainUI;
    public MainUI MainUI { get { return _mainUI; } }

    AudioSource _audioSource;
    public AudioSource AudioSource { get { return _audioSource; } }

    void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        _player = GetComponent<Player>();
        _audioSource = GetComponent<AudioSource>();

        _mainCamera = Camera.main;
    }

    public void Defeat()
    {
        SceneManager.LoadScene("MainMenu");
    }

}
