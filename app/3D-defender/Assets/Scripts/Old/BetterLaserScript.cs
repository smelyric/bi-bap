using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BetterLaserScript : MonoBehaviour
{

    public void SetPath(Vector3 start, Vector3 end)
    {
        Vector3 direction = end - start;

        transform.position = start + (direction / 2);
        transform.up = Quaternion.FromToRotation(Vector3.up, direction) * transform.up;

        SpriteRenderer[] spriteRenderers = GetComponentsInChildren<SpriteRenderer>();

        foreach (var spriteRenderer in spriteRenderers)
        {
            spriteRenderer.size = new Vector2(spriteRenderer.size.x, direction.magnitude / transform.localScale.y);
        }
    }
}
