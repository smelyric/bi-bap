using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class Utilities
{
    public static string ArrayToString<T>(T[] array)
    {
        string text = "{";
        foreach (var item in array)
        {
            text += item.ToString();
            text += ", ";
        }
        text += "}";
        return text;
    }

    private static Vector2 VisualElementShift(VisualElement element)
    {
        if (float.IsNaN(element.layout.width)) return Vector2.zero;

        return new Vector2(-element.layout.width / 2f, -element.layout.height / 2f);
    }

    public static void VisualElementTrackGameObject(VisualElement element, GameObject gameObject, Vector2 offset)
    {
        Vector3 screenPosition = Manager.Instance.MainCamera.WorldToScreenPoint(gameObject.transform.position);

        if (screenPosition.z < 0f)
        {
            element.visible = false;
            return;
        }

        //Debug.Log($"screenPosition {screenPosition}");

        element.visible = true;
        Vector2 panelPosition = RuntimePanelUtils.ScreenToPanel(element.panel, new Vector2(screenPosition.x, Screen.height - screenPosition.y - 1f));

        //Debug.Log($"panel = {panelPosition}");

        Vector2 shift = VisualElementShift(element);

        element.transform.position = panelPosition + shift + offset;
    }

    public static bool MouseRaycast(out RaycastHit hit, Camera camera, LayerMask layerMask, QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal)
    {
        Vector3 mousePos = Input.mousePosition;
        Ray mouseRay = camera.ScreenPointToRay(mousePos);

        if (Physics.Raycast(mouseRay, out hit, 1000f, layerMask, queryTriggerInteraction))
        {
            return true;
        }

        return false;
    } 
}
