using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

public class Settings : MonoBehaviour
{
    public static Settings Instance;

    [SerializeField] AudioSource _musicSource;

    // Start is called before the first frame update
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }

        else Destroy(gameObject);
    }

    public float MusicVolume
    {
        get {
            return _musicSource.volume;
        }

        set {
            _musicSource.volume = value;
        }
    }
}
