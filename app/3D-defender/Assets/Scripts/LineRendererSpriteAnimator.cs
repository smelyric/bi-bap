using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineRendererSpriteAnimator : MonoBehaviour
{
    [SerializeField] Sprite[] _sprites;
    Texture2D[] _textures;
    [SerializeField] LineRenderer _lineRenderer;
    int _frame = 0;
    [SerializeField] bool _rotate = false;

    // Start is called before the first frame update
    void Start()
    {
        if (_sprites == null || _sprites.Length == 0)
        {
            Debug.LogError("No sprites assigned!");
        }

        _textures = new Texture2D[_sprites.Length];

        int width = (int)_sprites[0].rect.width;
        int height = (int)_sprites[0].rect.height;

        Texture2D tile;

        for (int i = 0; i < _sprites.Length; i++)
        {
            Color[] pixels = _sprites[i].texture.GetPixels((int)_sprites[i].rect.x, (int)_sprites[i].rect.y, width, height);

            if (_rotate)
            {
                Color[] rotatedPixels = new Color[pixels.Length];

                for (int x = 0; x < (int)_sprites[i].rect.width; x++)
                {
                    for (int y = 0; y < (int)_sprites[i].rect.height; y++)
                    {
                        rotatedPixels[x * height + (height - 1 - y)] = pixels[x + y * width];
                    }
                }

                pixels = rotatedPixels;

                tile = new Texture2D(height, width, _sprites[i].texture.format, true, false);
            }

            else
            {
                tile = new Texture2D(width, height, _sprites[i].texture.format, true, false);
            }

            tile.SetPixels(pixels);
            tile.Apply();

            _textures[i] = tile;
        }

        Update();
    }

    // Update is called once per frame
    void Update()
    {
        _frame++;

        _frame = _frame % _textures.Length;

        _lineRenderer.material.mainTexture = _textures[_frame];
    }

    public void SetPath(Vector3 start, Vector3 end)
    {
        _lineRenderer.SetPosition(0, start);
        _lineRenderer.SetPosition(1, end);
    }

    public void AddSegment(Vector3 end)
    {
        _lineRenderer.positionCount++;
        _lineRenderer.SetPosition(_lineRenderer.positionCount - 1, end);
    }
}
