using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Sound
{
    [SerializeField] AudioClip _audioClip;
    [SerializeField, Range(0f, 1f)] float _volume;

    public void PlayAtPoint(Vector3 position)
    {
        AudioSource.PlayClipAtPoint(_audioClip, position, _volume);
    }

    public bool Playable
    {
        get
        {
            if (_audioClip == null) return false;
            else return true;
        }
    }
}
