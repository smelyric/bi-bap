using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class MainUI : MonoBehaviour
{
    VisualElement _root;

    Label _moneyAmountLabel;
    [SerializeField] GameObject _spawnTowerButtonPrefab;

    [SerializeField] GameObject _unitDescriptionPrefab;

    Label _splashTextLabel;
    float _splashTextLabelDuration = 0f;
    float _splashTextLabelElapsed = 0f;

    // Start is called before the first frame update
    void Start()
    {
        _root = GetComponent<UIDocument>().rootVisualElement;

        SetupBuyMenu();

        _moneyAmountLabel = _root.Q<Label>("moneyAmount");
        _splashTextLabel = _root.Q<Label>("splashText");

        _unitDescriptionPrefab = Instantiate(_unitDescriptionPrefab);
        _unitDescriptionPrefab.transform.SetParent(gameObject.transform);

        _root.Q<VisualElement>("unitMenuContainer").Add(_unitDescriptionPrefab.GetComponent<UIDocument>().rootVisualElement);
    }

    void Update()
    {
        _moneyAmountLabel.text = Manager.Instance.Player.GetMoney().ToString();

        UpdateSplashText();
    }


    public void SplashText(string text, float duration = 2f)
    {
        _splashTextLabel.text = text;
        _splashTextLabel.visible = true;
        _splashTextLabelDuration = duration;
        _splashTextLabelElapsed = 0f;
        //Debug.Log("Splashed '" + text + "'");
    }

    void UpdateSplashText()
    {
        if (_splashTextLabel.text == "") return;

        _splashTextLabelElapsed += Time.deltaTime;

        if (_splashTextLabelDuration < _splashTextLabelElapsed)
        {
            _splashTextLabel.text = "";
            _splashTextLabel.visible = false;
        }
    }

    void SetupBuyMenu()
    {
        BuildingSpawner towerSpawner = Manager.Instance.gameObject.GetComponent<BuildingSpawner>();
        var towerBox = _root.Q<GroupBox>("towerBox");
        for (int i = 0; i < towerSpawner.Prefabs.Length; i++)
        {
            int index = i;
            GameObject gameObjectButton = Instantiate(_spawnTowerButtonPrefab);
            gameObjectButton.transform.SetParent(gameObject.transform);
            Button button = gameObjectButton.GetComponent<UIDocument>().rootVisualElement.Q<Button>("button");
            button.text = towerSpawner.Prefabs[i].GetComponent<Unit>().UnitName + " - " + towerSpawner.Prefabs[i].GetComponent<Unit>().Value + " $";
            button.tooltip = towerSpawner.Prefabs[i].GetComponent<Unit>().UnitDestription;
            button.clicked += delegate{
                towerSpawner.SpawnTower(index);
            };
            
            towerBox.Add(button);
        }
    }
}
