using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Events;

public class BuildingSpawner : MonoBehaviour
{
    [SerializeField] GameObject[] _prefabs;
    public GameObject[] Prefabs { get {return _prefabs;}}

    int _spawning = -1;

    GameObject _grabbed = null;

    public UnityEvent StartedPlacing;

    // Update is called once per frame
    void Update()
    {
        if (_spawning == -1) return;

        if (_grabbed == null)
        {
            _grabbed = Instantiate(Prefabs[_spawning], Vector3.zero, Quaternion.identity);
            _grabbed.gameObject.GetComponent<Building>().Placing = true;
            StartedPlacing.Invoke();
        }

        if (Input.GetButtonDown("Fire2"))
        {
            _spawning = -1;
            Destroy(_grabbed);
            _grabbed = null;
            return;
        }

        RaycastHit hit;

        if (Utilities.MouseRaycast(out hit, Manager.Instance.MainCamera, 1 << 10))
        {
            Vector3 worldPos = hit.point;
            _grabbed.transform.position = worldPos;
        }

        if (Input.GetButtonDown("Fire1"))
        {
            // If player does not have enough money
            if (!Manager.Instance.Player.Buy(_grabbed.GetComponent<Unit>().Value))
            {
                Manager.Instance.MainUI.SplashText("Not enough money!");
                _spawning = -1;
                Destroy(_grabbed);
                _grabbed = null;
                return;
            }

            _spawning = -1;
            _grabbed.gameObject.GetComponent<Building>().Placing = false;
            _grabbed = null;
        }
    }

    public void SpawnTower(int index)
    {
        if (_spawning != -1) return;

        _spawning = index;
    }
}
