using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generation : MonoBehaviour
{
    GameObject _prefab;
    int _maxUnitCount;

    Vector3 _unitOriginPosition;

    float _maxMutationStrength = 0.1f;
    float _minMutationStrength = 0.01f;
    float _percentageToKeep = 0.201f;
    
    List<GameObject> _units;
    List<NeuralNetwork> _parentNeuralNetworks;

    System.Action _spawnUnitFunction;

    void Awake()
    {
        _units = new List<GameObject>();
        _parentNeuralNetworks = new List<NeuralNetwork>();

    }

    public static Generation BrandNewGeneration(GameObject prefab, int maxUnitCount, Vector3 unitOriginPosition)
    {
        Generation current = Manager.Instance.gameObject.AddComponent<Generation>();

        current._prefab = prefab;
        current._maxUnitCount = maxUnitCount;
        current._unitOriginPosition = unitOriginPosition;

        current._spawnUnitFunction = current.SpawnUnitRandom;

        return current;
    }

    public Generation BoreNewGeneration()
    {
        Generation newGeneration = Manager.Instance.gameObject.AddComponent<Generation>();

        newGeneration._prefab = _prefab;
        newGeneration._maxUnitCount = _maxUnitCount;
        newGeneration._unitOriginPosition = _unitOriginPosition;

        newGeneration._spawnUnitFunction = newGeneration.SpawnUnitFromParent;

        foreach (var unit in _units)
        {
            unit.GetComponent<NeuralBehavior>().EvaluateFitness();
        }

        SortUnits();

        float absoluteCountToKeep = Mathf.Max(1, (int)((float)_units.Count * _percentageToKeep));

        for (int i = 0; i < absoluteCountToKeep; i++)
        {
            newGeneration._parentNeuralNetworks.Add(_units[i].GetComponent<NeuralBehavior>().CloneNetwork());
        }

        return newGeneration;
    }

    void SortUnits()
    {
        _units.Sort(delegate(GameObject g1, GameObject g2){
            return g1.GetComponent<NeuralBehavior>().CompareTo(g2.GetComponent<NeuralBehavior>());
        });
    }

    void Update()
    {
        if (_units.Count < _maxUnitCount)
        {
            _spawnUnitFunction();
        }
    }

    void SpawnUnitFromParent()
    {
        GameObject unit = Instantiate(_prefab, _unitOriginPosition, Quaternion.identity);
        NeuralBehavior unitNeuralBehavior = unit.GetComponent<NeuralBehavior>();
        unitNeuralBehavior.Network = _parentNeuralNetworks[Random.Range(0, _parentNeuralNetworks.Count)].DeepCopy();
        unitNeuralBehavior.Network.Mutate(GetMutiationStrength());
        _units.Add(unit);
    }

    void SpawnUnitRandom()
    {
        GameObject unit = Instantiate(_prefab, _unitOriginPosition, Quaternion.identity);
        unit.GetComponent<NeuralBehavior>().GenerateNewNetwork();
        _units.Add(unit);
    }

    float GetMutiationStrength()
    {
        // Constant
        // float mutationStrength = _maxMutationStrength;

        // Linear function:
        // float mutationStrength = (((maxMutationStrength - minMutationStrength) / (float)maxCount) * units.Count) + minMutationStrength; 

        // Exponential function:
        float b = _minMutationStrength - 1f;
        float a = Mathf.Pow(_maxMutationStrength - b, 1f / _maxUnitCount);
        float mutationStrength = Mathf.Pow(a, _units.Count) + b;

        return mutationStrength;
    }

    public NeuralNetwork GetBestNetwork()
    {
        NeuralBehavior best = _units[0].GetComponent<NeuralBehavior>();

        foreach(var unit in _units)
        {
            NeuralBehavior other = unit.GetComponent<NeuralBehavior>();

            if (best.EvaluateFitness() < other.EvaluateFitness())
            {
                best = other;
            }
        }

        return best.CloneNetwork();
    }

    void OnDestroy()
    {
        foreach (var unit in _units)
        {
            Destroy(unit);
        }

        _units.Clear();
    }
}
