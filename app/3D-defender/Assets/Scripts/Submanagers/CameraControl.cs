using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CameraControl : MonoBehaviour
{
    [SerializeField] float _sensitivity;
    [SerializeField] float _speed;
    float _pitch;
    float _yaw;
    [SerializeField] float _pitchMin;
    [SerializeField] float _pitchMax;

    [SerializeField] float _heightMin;
    float _maxX = 500f;
    float _maxZ = 500f;

    void Start()
    {
        _pitch = gameObject.transform.eulerAngles.x;
        _yaw = gameObject.transform.eulerAngles.y;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Escape"))
        {
            if (Time.timeScale == 1f)
            {
                Time.timeScale = 0f;
                SceneManager.LoadScene("MainMenu", LoadSceneMode.Additive);
            }

            else
            {
                Time.timeScale = 1f;
                SceneManager.UnloadSceneAsync("MainMenu");
            }
        }

        if (Input.GetButton("Fire2"))
        {
            Rotation();
        }
        
        // WSAD
        float movementForward = Input.GetAxis("Vertical");
        float movementSideways = Input.GetAxis("Horizontal");
        float movementUpDown = Input.GetAxis("UpDown");

        gameObject.transform.Translate(new Vector3(transform.forward.x, 0f, transform.forward.z).normalized * _speed * movementForward, Space.World);
        gameObject.transform.Translate(Vector3.right * _speed * movementSideways);
        gameObject.transform.Translate(Vector3.up * _speed * movementUpDown, Space.World);

        CheckBounds();
    }

    void Rotation()
    {
        float rotationHorizontal = Input.GetAxis("Mouse X");
        float rotationVertical = Input.GetAxis("Mouse Y");

        _yaw += rotationHorizontal * _sensitivity;
        _pitch -= rotationVertical * _sensitivity;

        _yaw = _yaw % 360f;
        //Debug.Log("Yaw=" + yaw);
        _pitch = Mathf.Clamp(_pitch, _pitchMin, _pitchMax);

        gameObject.transform.eulerAngles = new Vector3(
            _pitch,
            _yaw,
            0f
        );
    }

    void CheckBounds()
    {
        if (gameObject.transform.position.y < _heightMin)
        {
            gameObject.transform.position = new Vector3(
                gameObject.transform.position.x,
                _heightMin,
                gameObject.transform.position.z
            );
        }

        if (gameObject.transform.position.x > _maxX)
        {
            gameObject.transform.position = new Vector3(
                _maxX,
                gameObject.transform.position.y,
                gameObject.transform.position.z
            );
        }

        else if (gameObject.transform.position.x < -_maxX)
        {
            gameObject.transform.position = new Vector3(
                -_maxX,
                gameObject.transform.position.y,
                gameObject.transform.position.z
            );
        }

        if (gameObject.transform.position.z > _maxZ)
        {
            gameObject.transform.position = new Vector3(
                gameObject.transform.position.z,
                gameObject.transform.position.y,
                _maxZ
            );
        }

        else if (gameObject.transform.position.z < -_maxZ)
        {
            gameObject.transform.position = new Vector3(
                gameObject.transform.position.z,
                gameObject.transform.position.y,
                -_maxZ
            );
        }
    }
}
