using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] GameObject _spawnerPrefab;
    [SerializeField] GameObject _enemyPrefab;
    [SerializeField] float _spawnTimer;
    [SerializeField, Range(1f, 2f)] float _enemyHealthMultiplier;
    float _sinceLastSpawn = 0;
    [SerializeField] int _enemiesInGeneration;
    int _generationNumber = 0;
    Generation _currentGeneration = null;
    [SerializeField] Vector3 _enemyOriginPosition;

    void Reset()
    {
        _spawnTimer = 20f;
        _enemiesInGeneration = 20;
        _enemyHealthMultiplier = 1f;
    }

    // Start is called before the first frame update
    void Start()
    {
        _currentGeneration = Generation.BrandNewGeneration(_enemyPrefab, _enemiesInGeneration, _enemyOriginPosition);
        Instantiate(_spawnerPrefab, _enemyOriginPosition + new Vector3(0f, 15f, 0f), Quaternion.identity);
    }

    // Update is called once per frame
    void Update()
    {
        _sinceLastSpawn += Time.deltaTime;

        if (_sinceLastSpawn > _spawnTimer) SpawnNextGeneration();
    }

    void SpawnNextGeneration()
    {
        _enemyPrefab.GetComponent<Health>().Amount *= _enemyHealthMultiplier;
        Generation newGeneration = _currentGeneration.BoreNewGeneration();
        Destroy(_currentGeneration);
        _currentGeneration = newGeneration;

        _sinceLastSpawn = 0f;
        _generationNumber++;
        Manager.Instance.MainUI.SplashText("Generation " + (_generationNumber + 1), 2f);
    }
}
