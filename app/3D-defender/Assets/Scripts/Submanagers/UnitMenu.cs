using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class UnitMenu : MonoBehaviour
{
    [SerializeField] LayerMask _selectableLayers;
    Unit _unitDescribed = null;

    VisualElement _root;
    Label _name;
    Label _health;
    Label _description;
    Button _upgrade;

    // Start is called before the first frame update
    void Start()
    {
        _root = GetComponent<UIDocument>().rootVisualElement;
        _root.visible = false;

        _name = _root.Q<Label>("unitName");
        _health = _root.Q<Label>("health");
        _description = _root.Q<Label>("unitDescription");
        _upgrade = _root.Q<Button>("upgrade");
        _upgrade.style.display = DisplayStyle.None;

        Manager.Instance.GetComponent<BuildingSpawner>().StartedPlacing.AddListener(Deselect);
    }

    void Deselect()
    {
        _unitDescribed = null;
        _root.visible = false;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            RaycastHit hit;

            if (Utilities.MouseRaycast(out hit, Manager.Instance.MainCamera, _selectableLayers))
            {
                _unitDescribed = hit.collider.gameObject.GetComponent<Unit>();
                CheckUpgrade();
            }

            else _unitDescribed = null;
        }

        if (_unitDescribed != null)
        {
            _root.visible = true;
            UpdateMenu();
        }

        else _root.visible = false;
    }

    private void CheckUpgrade()
    {
        if (_unitDescribed is Building)
        {
            Building building = _unitDescribed as Building;

            if (building.Upgradable())
            {
                _upgrade.text = $"Upgrade: {building.UpgradeCost()}$";
                _upgrade.style.display = DisplayStyle.Flex;

                Action click = delegate {
                    GameObject tryUpgrade = building.TryUpgrade();
                    if (tryUpgrade == null) return;

                    _unitDescribed = tryUpgrade.GetComponent<Unit>();
                    CheckUpgrade();
                };

                _upgrade.clickable = new Clickable(click);
                return;
            }
        }
        
        _upgrade.style.display = DisplayStyle.None;
    }

    private void UpdateMenu()
    {
        _name.text = _unitDescribed.UnitName;
        _health.text = _unitDescribed.Health.Amount.ToString();
        _description.text = _unitDescribed.UnitDestription;
    }
}
