using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] long _money;
    [SerializeField] GameObject _mainBase;
    [SerializeField] bool _unlimitedMoney;
    float _gameStartTime;

    // Start is called before the first frame update
    void Start()
    {
        if (_unlimitedMoney)
        {
            _money = 1_000_000_000_000_000L;
        }

        _mainBase.GetComponent<Health>().Died.AddListener(BaseDied);

        _gameStartTime = Time.realtimeSinceStartup;
    }

    void Update()
    {
        if (Input.GetButtonDown("Money"))
        {
            Income(1000L);
        }
    }

    public bool Buy(long cost)
    {
        if (_money < cost) return false;

        _money -= cost;
        //Debug.Log("Player now has this much money: " + money);
        return true;
    }

    public void Income(long value)
    {
        //Debug.Log(value);
        _money += value;
    }

    public long GetMoney()
    {
        return _money;
    }

    void BaseDied()
    {
        Manager.Instance.MainUI.SplashText($"Your base has been destroyed. You survived for {Time.realtimeSinceStartup - _gameStartTime} seconds!", 3f);
        Manager.Instance.Invoke("Defeat", 3f);
    }
}
