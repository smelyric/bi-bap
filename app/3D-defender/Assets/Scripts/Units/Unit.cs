using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Health))]
public class Unit : MonoBehaviour
{
    [SerializeField] protected long _value;
    [SerializeField] protected string _unitName;
    [SerializeField] protected string _unitDescription;

    protected Health _health;
    public Health Health { get {return _health;}}

    public long Value { get { return _value; } }
    public string UnitName { get { return _unitName; } }
    public string UnitDestription { get { return _unitDescription; } }

    virtual protected void Start()
    {
        _health = GetComponent<Health>();
    }
}