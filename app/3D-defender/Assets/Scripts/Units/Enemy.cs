using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(NeuralEnemyMovement))]
[RequireComponent(typeof(Health))]
[RequireComponent(typeof(BoxCollider))]
public class Enemy : Unit
{
    [SerializeField] private float _crashDamage;
    private NeuralEnemyMovement _neuralMovement;
    private BoxCollider _boxCollider;
    [SerializeField] LayerMask _targetLayerMask;

    // Start is called before the first frame update
    override protected void Start()
    {
        base.Start();

        _neuralMovement = GetComponent<NeuralEnemyMovement>();
        _health.Died.AddListener(OnDeath);
        _boxCollider = GetComponent<BoxCollider>();
    }

    // Update is called once per frame
    void Update()
    {
        Collider[] hits = Physics.OverlapBox(transform.position, transform.localScale / 2f, Quaternion.identity, _targetLayerMask);

        if (hits.Length != 0)
        {
            HitBase(hits[0].gameObject);
        }
    }

    void HitBase(GameObject baseGameObject)
    {
        // Enemy hit base
        baseGameObject.gameObject.GetComponent<Health>().TakeDamage(_crashDamage);
        _neuralMovement.HitBase();
        gameObject.SetActive(false);
    }

    void OnDeath()
    {
        Manager.Instance.Player.Income(_value);
    }
}
