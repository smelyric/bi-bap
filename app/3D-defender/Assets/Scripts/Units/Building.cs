using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Renderer))]
public class Building : Unit
{
  Renderer _renderer;
  bool _placing = false;
  [SerializeField, Tooltip("Leave blank if building has no possible upgrade.")] GameObject _upgrade;

  public bool Placing
  {
    get { return _placing; }
    set { SetPlacing(value); }
  }

  public bool Upgradable()
  {
    if (_upgrade == null) return false;

    return true;
  }

  public long UpgradeCost()
  {
    return _upgrade.GetComponent<Unit>().Value;
  }

  public GameObject TryUpgrade()
  {
    if (!Manager.Instance.Player.Buy(_upgrade.GetComponent<Unit>().Value))
    {
      Manager.Instance.MainUI.SplashText("Not enough money!");
      return null;
    }

    Destroy(gameObject);
    return Instantiate(_upgrade, transform.position, Quaternion.identity);
  }

  void SetPlacing(bool placing)
  {
    if (_renderer == null) _renderer = GetComponent<Renderer>();

    if (_placing == placing) return;

    else if (placing)
    {
      SetAttackersEnabled(false);
      SetRangeIndicatorsEnabled(true);
      _renderer.material.color = new Color(0f, 1f, 0f, 0.5f); 
    }

    else
    {
      SetAttackersEnabled(true);
      SetRangeIndicatorsEnabled(false);
      _renderer.material.color = new Color(1f, 1f, 1f, 1f); 
    }

    _placing = placing;
  }

  void SetAttackersEnabled(bool enable)
  {
    Attacker[] attackers = GetComponents<Attacker>();

    foreach (var attacker in attackers)
    {
      attacker.enabled = enable;
    }
  }

  void SetRangeIndicatorsEnabled(bool enable)
  {
    Attacker[] attackers = GetComponents<Attacker>();

    foreach (var attacker in attackers)
    {
      attacker.RangeIndicator.SetActive(enable);
    }
  }
}
