using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Attacker : MonoBehaviour
{
  [HideInInspector] public UnityEvent Attacked;

  [SerializeField] protected LayerMask _attackLayer;

  [SerializeField] protected float _range;

  GameObject _rangeIndicator;
  [SerializeField] Sound _attackSound;

  virtual protected void Awake()
  {
    _rangeIndicator = Instantiate(Manager.Instance.RangeIndicatorPrefab);
    _rangeIndicator.transform.SetParent(transform, false);
    _rangeIndicator.transform.localScale = new Vector3(_range, _range, _range);
    _rangeIndicator.SetActive(false);
  }

  virtual protected void Start()
  {
    if (_attackSound != null) Attacked.AddListener(delegate { _attackSound.PlayAtPoint(transform.position); });
  }

  protected List<GameObject> GetTargetsInRange()
  {
    Collider[] hits = Physics.OverlapSphere(this.transform.position, _range, _attackLayer);

    List<GameObject> targets = new List<GameObject>();

    foreach (var hit in hits)
    {
      targets.Add(hit.gameObject);
    }

    return targets;
  }

  public GameObject RangeIndicator { get { return _rangeIndicator; } }
}
