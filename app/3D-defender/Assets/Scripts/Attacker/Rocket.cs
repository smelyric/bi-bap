using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocket : Projectile
{
    Vector3 _direction;
    bool _targetSet = false;
    [SerializeField] LayerMask _triggerLayer;
    [SerializeField] LayerMask _explosionLayer;
    [SerializeField] float _speed;
    [SerializeField] float _damage;
    [SerializeField] float _explosionRadius;
    [SerializeField] GameObject _explosionPrefab;

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    void Move()
    {
        if (!_targetSet) return;

        //Debug.Log($"_direction={_direction}, _speed={_speed}, deltatime={Time.deltaTime}");

        gameObject.transform.Translate(_direction * _speed * Time.deltaTime, Space.World);
    }

    void OnTriggerEnter(Collider other)
    {
        if ((1 << other.gameObject.layer & _triggerLayer.value) == 0) return;

        //Debug.Log($"Collided with {other.gameObject.name}");

        Explode();
    }

    void Explode()
    {
        Impacted.Invoke();

        Collider[] hits = Physics.OverlapSphere(this.transform.position, _explosionRadius, _explosionLayer);

        foreach (var hit in hits)
        {
            hit.gameObject.GetComponent<Health>().TakeDamage(_damage);
        }

        GameObject explosion = Instantiate(_explosionPrefab, gameObject.transform.position, Quaternion.identity);
        explosion.transform.localScale = new Vector3(_explosionRadius / 10f, _explosionRadius / 10f, _explosionRadius / 10f);

        Destroy(gameObject);
    }

    public void SetTarget(Vector3 target)
    {
        _targetSet = true;
        _direction = (target - gameObject.transform.position).normalized;
        gameObject.transform.LookAt(target);
    }
}
