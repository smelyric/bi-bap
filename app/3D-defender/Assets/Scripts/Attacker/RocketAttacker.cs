using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketAttacker : ProjectileAttacker
{
    [SerializeField] float _attackDelay;
    [SerializeField] float _rocketOriginHeight;

    GameObject _currentRocket;

    float _timeSinceAttack = 0;
    float _rocketRespawnDelay;

    override protected void Start()
    {
        base.Start();

        _rocketRespawnDelay = _attackDelay / 2f;
        NewRocket();
    }

    void NewRocket()
    {
        _currentRocket = Instantiate(_projectilePrefab, new Vector3(0f, _rocketOriginHeight, 0f), Quaternion.identity);
        _currentRocket.transform.SetParent(transform, false);
    }

    // Update is called once per frame
    void Update()
    {
        _timeSinceAttack += Time.deltaTime;

        if (_timeSinceAttack < _attackDelay) return;

        List<GameObject> targets = GetTargetsInRange();

        if (targets.Count == 0) return;

        _timeSinceAttack = 0f;
        Attack(targets[0]);
    }

    void Attack(GameObject target)
    {
        _currentRocket.GetComponent<Rocket>().SetTarget(target.gameObject.transform.position);

        Invoke("NewRocket", _attackDelay / 2f);
    }
}
