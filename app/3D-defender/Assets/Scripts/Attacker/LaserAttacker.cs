using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserAttacker : HitscanAttacker
{
    [SerializeField] GameObject _laserPrefab;
    [SerializeField] float _attackDelay;
    [SerializeField] float _multishot;
    [SerializeField] float _laserOriginHeight;
    float _timeSinceAttack = 0;

    // Update is called once per frame
    void Update()
    {
        _timeSinceAttack += Time.deltaTime;

        if (_timeSinceAttack < _attackDelay) return;

        List<GameObject> targets = GetTargetsInRange();

        if (targets.Count == 0) return;

        _timeSinceAttack = 0f;
        Attack(targets);
    }

    void Attack(List<GameObject> targets)
    {
        Attacked.Invoke();

        GameObject laser = Instantiate(_laserPrefab);
        LineRendererSpriteAnimator laserScript = laser.GetComponent<LineRendererSpriteAnimator>();
        laserScript.SetPath(transform.position + new Vector3(0f, _laserOriginHeight, 0f), targets[0].transform.position);
        Destroy(laser, 0.2f);

        targets[0].GetComponent<Health>().TakeDamage(_damage);

        for (int i = 1; i < targets.Count && i < _multishot; i++)
        {
            laserScript.AddSegment(targets[i].transform.position);
            targets[i].GetComponent<Health>().TakeDamage(_damage);
        }
    }

    void Attack(GameObject target)
    {
        List<GameObject> targets = new List<GameObject>();
        targets.Add(target);
        Attack(targets);
    }
}
