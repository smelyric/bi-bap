using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Projectile : MonoBehaviour
{
    [HideInInspector] public UnityEvent Impacted;

    [SerializeField] Sound _impactSound;

    protected void Start()
    {
        if (_impactSound != null) Impacted.AddListener(delegate{_impactSound.PlayAtPoint(transform.position);});
    }
}
