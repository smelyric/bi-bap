using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Light))]
public class ExplosionFlash : MonoBehaviour
{
    Light _light;

    // Start is called before the first frame update
    void Start()
    {
        _light = GetComponent<Light>();
        _light.range *= transform.parent.localScale.x;

        StartCoroutine(Fade());
    }

    IEnumerator Fade()
    {
        yield return new WaitForSeconds(0.5f);

        while (_light.range > 0f)
        {
            _light.range *= 0.8f;
            yield return new WaitForSeconds(0.05f);
        }
    }
}
