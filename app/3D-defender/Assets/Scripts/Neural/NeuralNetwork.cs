using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Neural network class
public class NeuralNetwork
{
    float[][] _values;
    float[][] _biases;
    float[][][] _weights;

    public int[] Shape {
        get {
            if (_values == null) return null;

            int[] shape = new int[_values.Length];

            for (int i = 0; i < _values.Length; i++)
            {
                shape[i] = _values[i].Length;
            }

            return shape;
        }
    }

    public NeuralNetwork()
    {
    }

    public NeuralNetwork(int[] nodeCount)
    {
        Generate(nodeCount);
    }

    public void Generate(int[] nodeCount)
    {
        AllocateValues(nodeCount);
        InitBiases(nodeCount);
        InitWeights(nodeCount);
    }

    void AllocateValues(int[] nodeCount)
    {
        _values = new float[nodeCount.Length][];

        for (int i = 0; i < nodeCount.Length; i++)
        {
            _values[i] = new float[nodeCount[i]];
        }
    }

    void InitBiases(int[] nodeCount)
    {
        _biases = new float[nodeCount.Length][];

        for (int i = 0; i < nodeCount.Length; i++)
        {
            _biases[i] = new float[nodeCount[i]];

            for (int j = 0; j < nodeCount[i]; j++)
            {
                _biases[i][j] = Random.Range(-0.5f, 0.5f);
            }
        }
    }

    void InitWeights(int[] nodeCount)
    {
        if (nodeCount.Length == 0)
        {
            return;
        }

        _weights = new float[nodeCount.Length - 1][][];

        for (int i = 1; i < nodeCount.Length; i++)
        {
            _weights[i - 1] = new float[nodeCount[i]][];

            for (int j = 0; j < nodeCount[i]; j++)
            {
                _weights[i - 1][j] = new float[nodeCount[i - 1]];

                for (int k = 0; k < nodeCount[i - 1]; k++)
                {
                    _weights[i - 1][j][k] = Random.Range(-0.5f, 0.5f);
                }
            }
        }
    }

    public float[] Evaluate(float[] inputs)
    {
        if (inputs.Length != _values[0].Length)
        {
            Debug.Log("Input size mismatch!");
        }

        // Fill input neurons
        for (int i = 0; i < inputs.Length; i++)
        {
            _values[0][i] = inputs[i];
        }

        // For each layer - except last, compute values
        for (int i = 1; i < _values.Length - 1; i++)
        {
            // For each neuron
            for (int j = 0; j < _values[i].Length; j++)
            {
                float value = _biases[i][j];

                for (int k = 0; k < _values[i - 1].Length; k++)
                {
                    value += _weights[i - 1][j][k] * _values[i - 1][k];
                }

                _values[i][j] = ActivationFunction(value);
            }
        }

        // For last layer
        int lastLayer = _values.Length - 1;
        for (int j = 0; j < _values[lastLayer].Length; j++)
        {
            float value = _biases[lastLayer][j];

            for (int k = 0; k < _values[lastLayer - 1].Length; k++)
            {
                value += _weights[lastLayer - 1][j][k] * _values[lastLayer - 1][k];
            }

            _values[lastLayer][j] = System.MathF.Tanh(value); // Last activation function
        }

        return _values[lastLayer];
    }

    private float ActivationFunction(float value)
    {
        // return System.MathF.Tanh(value)

        // Leaky RELU
        if (value > 0f)
        {
            return value;
        }

        return value / 50f;
    }

    override public string ToString()
    {
        string text = "";
        text += "Values:" + System.Environment.NewLine;

        foreach (var valueColumn in _values)
        {
            foreach (var value in valueColumn)
            {
                text += value.ToString("0.000000");
                text += ", ";
            }

            text += System.Environment.NewLine;
        }

        text += "Biases:" + System.Environment.NewLine;

        foreach (var biasColumn in _biases)
        {
            foreach (var bias in biasColumn)
            {
                text += bias.ToString("0.000000");
                text += ", ";
            }

            text += System.Environment.NewLine;
        }

        text += "Weights:" + System.Environment.NewLine;

        foreach (var weightColumn in _weights)
        {
            foreach (var weightRow in weightColumn)
            {
                foreach (var weight in weightRow)
                {
                    text += weight.ToString("0.000000");
                    text += ", ";
                }
                text += "-->";
            }
            text += System.Environment.NewLine;
        }

        return text;
    }

    public void Mutate(float strength, float chanceToReset = 0f)
    {
        for (int i = 0; i < _biases.Length; i++)
        {
            for (int j = 0; j < _biases[i].Length; j++)
            {
                _biases[i][j] += Random.Range(-strength, strength);
            }
        }

        for (int i = 0; i < _weights.Length; i++)
        {
            for (int j = 0; j < _weights[i].Length; j++)
            {
                for (int k = 0; k < _weights[i][j].Length; k++)
                {
                    _weights[i][j][k] += Random.Range(-strength, strength);
                }
            }
        }
    }

    public NeuralNetwork DeepCopy()
    {
        NeuralNetwork clone = new NeuralNetwork();

        // Cloning values
        clone._values = new float[_values.Length][];

        for (int i = 0; i < _values.Length; i++)
        {
            clone._values[i] = new float[_values[i].Length];

            for (int j = 0; j < _values[i].Length; j++)
            {
                clone._values[i][j] = _values[i][j];
            }
        }

        // Cloning biases
        clone._biases = new float[_biases.Length][];

        for (int i = 0; i < _biases.Length; i++)
        {
            clone._biases[i] = new float[_biases[i].Length];

            for (int j = 0; j < _biases[i].Length; j++)
            {
                clone._biases[i][j] = _biases[i][j];
            }
        }

        // Cloning weights
        clone._weights = new float[_weights.Length][][];

        for (int i = 0; i < _weights.Length; i++)
        {
            clone._weights[i] = new float[_weights[i].Length][];

            for (int j = 0; j < _weights[i].Length; j++)
            {
                clone._weights[i][j] = new float[_weights[i][j].Length];

                for (int k = 0; k < _weights[i][j].Length; k++)
                {
                    clone._weights[i][j][k] = _weights[i][j][k];
                }
            }
        }

        return clone;
    }
}

