using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeuralEnemyMovement : NeuralBehavior
{
    [Header("Stats")]
    [SerializeField] float _speed;
    [SerializeField, Tooltip("In degrees per second")] float _maxTurningSpeed;
    private Vector3 _direction;

    private bool _hitBase = false;

    private float _bornAt = 0f;
    private float _hitBaseAt = 0f;



    void Start()
    {
        // Default direction is to base
        _direction = new Vector3(-transform.position.x, 0f, -transform.position.z).normalized;
        _bornAt = Time.time;
    }

    override protected float[] GetInput()
    {
        Vector2 directionToEnemy = new Vector2(-transform.position.x, -transform.position.z);


        float angle = Vector2.SignedAngle(directionToEnemy, new Vector2(_direction.x, _direction.z));

        //Debug.Log("Direction to enemy = " + directionToEnemy + ", direction = " + direction + ", angle = " + angle);

        float[] input = {
            transform.position.x / 200f,
            transform.position.z / 200f,
            angle / 180f,
            transform.position.magnitude / 200f
        };

        //Debug.Log(Utilities.ArrayToString<float>(input));

        return input;
    }

    public void HitBase()
    {
        _hitBase = true;
        _hitBaseAt = Time.time;
    }

    override public float EvaluateFitness()
    {
        if (!_hitBase)
        {
            _fitness = Mathf.Max(_fitness, -transform.position.magnitude);
        }

        else
        {
            _fitness = 1 / (_hitBaseAt - _bornAt);
        }

        return _fitness;
    }

    override protected void Behave(float[] netoworkResult)
    {
        _direction = Quaternion.AngleAxis(netoworkResult[0] * _maxTurningSpeed * Time.deltaTime, Vector3.up) * _direction;
        transform.rotation = Quaternion.FromToRotation(Vector3.forward, _direction);
        transform.Translate(_direction * _speed * Time.deltaTime, Space.World);
    }
}
