using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class NeuralBehavior : MonoBehaviour
{
    [SerializeField] int[] _networkShape;
    protected float _fitness;
    protected NeuralNetwork _network;

    public NeuralNetwork Network
    {
        get{ return _network; }
        set{ _network = value; }
    }

    void Awake()
    {
        _fitness = float.NegativeInfinity;
    }

    // Update is called once per frame
    void Update()
    {
        float[] input = GetInput();
        float[] output = _network.Evaluate(input);
        Behave(output);
    }

    public void GenerateNewNetwork()
    {
        _network = new NeuralNetwork(_networkShape);
    }

    abstract public float EvaluateFitness();
    abstract protected float[] GetInput();
    abstract protected void Behave(float[] netoworkResult);

    public int CompareTo(object obj)
    {
        NeuralBehavior other = obj as NeuralBehavior;

        //if (other == null) throw new System.ArgumentException("Object is not a NeuralBehavior!");

        return -this._fitness.CompareTo(other._fitness);
    }

    public NeuralNetwork CloneNetwork()
    {
        return _network.DeepCopy();
    }

    public void SetFitness(float inFitness)
    {
        _fitness = inFitness;
    }
}
