using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UIElements;

public class Health : MonoBehaviour
{
    [HideInInspector] public UnityEvent Died;

    [SerializeField] float _amount;
    float _amountMax;
    [Tooltip("How much health per second the enemy loses.")]
    [SerializeField] float _healthDecayRate;
    [SerializeField] bool _canTakeDamage;
    [SerializeField] GameObject _healthbarGameObject;
    ProgressBar _healthbar;
    [SerializeField] bool _alwaysShowHealthbar;
    [SerializeField] Sound _deathSound;

    [SerializeField] GameObject _spawnAfterDeath;

    float _damageRecolorRemaining = float.NegativeInfinity;

    public float Amount { get {return _amount;} set {_amount = value;} }

    void Awake()
    {
        _amountMax = _amount;
    }

    void Start()
    {
        if (_deathSound.Playable) Died.AddListener(delegate{ _deathSound.PlayAtPoint(transform.position);});

        if (_healthbarGameObject != null)
        {
            _healthbarGameObject = Instantiate(_healthbarGameObject);
            _healthbarGameObject.transform.SetParent(gameObject.transform);
            var root = _healthbarGameObject.GetComponent<UIDocument>().rootVisualElement;
            _healthbar = root.Q<ProgressBar>("healthbar");
            _healthbar.SetEnabled(false);

            if (!_alwaysShowHealthbar)
            {
                _healthbar.visible = false;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        _amount -= _healthDecayRate * Time.deltaTime;

        if (_damageRecolorRemaining > 0f)
        {
            _damageRecolorRemaining -= Time.deltaTime;
            
            if (_damageRecolorRemaining < 0f)
            {
                GetComponent<Renderer>().material.color = new Color(1f, 1f, 1f, 1f);
            }
        }
    }

    void LateUpdate()
    {
        if (_healthbar != null)
        {
            UpdateHealthbar();
        }

        if (_amount <= 0f)
        {
            Die();
        }
    }

    void UpdateHealthbar()
    {
        if (_amountMax == _amount && !_alwaysShowHealthbar)
        {
            _healthbar.visible = false;
            return;
        }

        _healthbar.visible = true;
        Utilities.VisualElementTrackGameObject(_healthbar, gameObject, new Vector2(0f, 30f));
        _healthbar.value = _amount / _amountMax;
    }

    public void TakeDamage(float damage)
    {
        if (!_canTakeDamage)
        {
            return;
        }

        _amount -= damage;
        GetComponent<Renderer>().material.color = new Color(1f, 0f, 0f, 1f);
        _damageRecolorRemaining = 0.2f;
    }

    void Die()
    {
        Died.Invoke();
        gameObject.SetActive(false);

        if (_spawnAfterDeath != null)
        {
            Instantiate(_spawnAfterDeath, transform.position, Quaternion.identity);
        }
    }

    void OnDestroy()
    {
        if (_healthbar != null)
        {
            Destroy(_healthbarGameObject);
        }
    }
}
