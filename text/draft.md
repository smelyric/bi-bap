https://towardsdatascience.com/building-a-neural-network-framework-in-c-16ef56ce1fef

https://www.kaggle.com/questions-and-answers/197627

neuronova sit s adaptivnim dotahovanim informaci, kdyz je neuron nula, tak nedotahuje

https://sfbgames.itch.io/chiptone

Stable Diffusion

[RequireComponent(typeof(AudioSync))]

Méně mutace (nebo žádná), pokud zasáhnou bázi.

Odebírání HP, když jedou pryč od středu. Fitness i podle toho, jak dlouho přežili.

Vylepšení, multithread, výpočet na GPU.

---

---

## Zadání

### Strategická 3D hra se strojovým učením

#### Strategic 3D game with machine learning

Cílem práce je navrhnout a vytvořit jednoduchou strategickou 3D hru, která bude využívat principy strojového učení. Součástí práce je i vytvoření sady 3D modelů.

1. Analyzujte dostupné hry využívající strojové učení.
1. Analyzujte, jaké modely strojového učení lze v této situaci použít.
1. Analyzujte dostupné 3D herní enginy a na základě analýzy vhodný vyberte.
1. Pomocí nástrojů softwarového inženýrství navrhněte prototyp hry včetně uživatelského rozhraní. 
1. Implementujte hru s použitím vlastních 3D modelů.
1. Proveďte vhodné testování.

---

### České vysoké učení technické v Praze
### Fakulta informačních technologií
### Katedra softwarového inženýrství

#### Obor: Webové a softwarové inženýrství
#### Zaměření: Počítačová grafika

# Strategická 3D hra se strojovým učením
## Bakalářská práce

#### Vypracoval: Richard Smělý
#### Vedoucí: Ing. Petr Pauš, Ph.D.

##### 2022

---

## Čestné prohlášení

Prohlašuji, že jsem předloženou práci vypracoval samostatně a že jsem uvedl veškeré
použité informační zdroje v souladu s Metodickým pokynem o dodržování etických
principů při přípravě vysokoškolských závěrečných prací.

Beru na vědomí, že se na moji práci vztahují práva a povinnosti vyplývající ze zákona
č. 121/2000 Sb., autorského zákona, ve znění pozdějších předpisů, zejména
skutečnost, že České vysoké učení technické v Praze má právo na uzavření licenční
smlouvy o užití této práce jako školního díla podle § 60 odst. 1 citovaného zákona.

V Praze dne xx.xx.20xx

Jméno a příjmení studenta

## Poděkování

Děkuji Ing. Petr Paušovi, Ph.D. za odborné vedení práce, věcné připomínky, dobré rady a vstřícnost při konzultacích a vypracovávání bakalářské práce.

## Abstrakt

Cílem této práce...

## Abstract

The aim of this work...

## Obsah

1. Úvod
1. Analýza her využívajících strojové učení
1. Analýza modelů strojového učení
1. Analýza a výběr 3D herních enginů
1. Návrh prototypu hry

    1. Něco
    1. Uživatelské rozhraní

1. Implementace hry
1. Testování
1. Závěr
1. Zdroje

    1. Seznam literatury
    1. Internetové zdroje

1. Citace
1. Seznam obrázků a tabulek

---

## Úvod

## Analýza her využívající strojové učení

## Analýza modelů strojového učení

## Analýza a výběr 3D herních enginů

## Návrh prototypu hry

## Implementace hry

## Testování

## Závěr

## Zdroje

### Seznam literatury

### Internetové zdroje

## Citace

## Seznam obrázků a tabulek
